<?php

/**
 * Media Types Controller . 
 * Add / Edit / Delete / View Media Types
 * @author mohsintech@gmail.
 */
class MediaTypesController extends AppController {

    public $name = 'MediaTypes';
    public $uses = array('MediaType');
    public $helpers=array('Paginator');
    var $paginate = array(
        'limit' => 10,
        'page' => 1,
        'order' => array('MediaType.id' => 'asc')
    );

    /**
     * Base parent before filter 
     */
    public function beforeFilter() {
        parent::beforeFilter();
    }

    /**
     *  View all Media Types with paginations
     */
    public function admin_index() {
        $this->paginate = array('order' => array('MediaType.id' => 'ASC'));
        $mediaTypes = $this->paginate('MediaType');
        $this->set(compact('mediaTypes'));
    }

    /**
     * Add new Media Types 
     */
    public function admin_add() {
        if ($this->request->isPost()) {
            if ($this->MediaType->save($this->data)) {
                $this->Session->setFlash('Media Types added successfully', 'success');
                $this->redirect('/admin/media_types/');
            } else {
                $this->Session->setFlash('Please enter correct information', 'error');
            }
        }
    }
    
    /** 
     * Update Media Types
     * @param int $id 
     */

    public function admin_edit($id) {
        if (empty($id)) {
            $this->Session->setFlash('Invid Media Types Id', 'error');
            $this->redirect('/admin/media_types/');
        }
        if (!empty($this->data)) {
            if ($this->MediaType->save($this->data)) {
                $this->Session->setFlash('Media Types added successfully', 'success');
                $this->redirect('/admin/media_types/');
            } else {
                $this->Session->setFlash('Please enter correct information', 'error');
            }
        }
        $this->data = $this->MediaType->findById($id);
    }

    /**
     * Remove Media Types
     * @param int $id 
     */
    public function admin_delete($id) {
        if ($this->MediaType->delete($id)) {
            $this->Session->setFlash('Media Types Removed successfully', 'success');
            $this->redirect('/admin/media_types/');
        } else {
            $this->Session->setFlash('Error on removing Media Types ', 'error');
            $this->redirect('/admin/media_types/');
        }
    }

}
