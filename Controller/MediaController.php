<?php

class MediaController extends AppController {

    public $name = 'Media';
    public $uses = array('Media', 'MediaType');
    public $helpers = array('Paginator');
    var $paginate = array(
        'limit' => 10,
        'page' => 1,
        'order' => array('Media.id' => 'asc')
    );

   
    public function admin_index() {
        if(!empty($this->data)){
            $this->Session->delete('media_types');
            foreach($this->data['Media']['type'] as $type){
                if(!empty($type))
                    $media_type[]=$type;
            }
            $this->Session->write('media_types',$media_type);
        }
        
        $checkedMediaTtypes=$this->Session->read('media_types');
        if (empty($checkedMediaTtypes)) {
            $media = $this->paginate('Media');
        } else {
            $media = $this->paginate('Media', array('Media.media_type_id' => $checkedMediaTtypes));
        }
        $mediaTypes = $this->MediaType->find('list');
        $this->set(compact('mediaTypes','checkedMediaTtypes'));
        $this->set('media', $media);
    }

    /**
     * Select Media Type
     */
    public function admin_add_stepone() {
        if (!empty($this->data)) {
            $type_id = $this->data['Media']['type_id'];
            $this->redirect('/admin/media/add/' . $type_id);
        }
        $mediaTypes = $this->MediaType->find('list');
        $this->set(compact('mediaTypes'));
    }

    /**
     * sorting , paging on media
     * @param int $type_id 
     */
    public function admin_add($type_id=null) {
        if (!empty($this->data)) {
            if ($this->Media->save($this->data)) {
                $this->Session->setFlash('Media Saved Successfully', 'success');
                $this->redirect('/admin/media');
            } else {
                $this->Session->setFlash('Error on adding Media', 'error');
            }
            $this->set('mediaTypeId', $this->data['Media']['media_type_id']);
        } else {
            $this->set('mediaTypeId', $type_id);
        }
    }

    /**
     * Update Media 
     * @param int $id 
     */
    public function admin_edit($id=null) {
        if (!empty($this->data)) {
            if ($this->Media->save($this->data)) {
                $this->Session->setFlash('Media Saved Successfully', 'success');
                $this->redirect('/admin/media');
            } else {
                $this->Session->setFlash('Error on adding Media', 'error');
            }
        }
        $this->data = $this->Media->findById($id);
    }

    /**
     * Remove Media Types
     * @param int $id
     * @todo Remove Media Related files and data also 
     */
    public function admin_delete($id) {
        if ($this->Media->delete($id)) {
            $this->Session->setFlash('Media Deleted.', 'success');
        } else {
            $this->Session->setFlash('Media Deleted.', 'error');
        }
        $this->redirect('/admin/media/');
    }

}
