<?php
/**
 *  Core Controller . All controllers will extent this 
 */
class AppController extends Controller {

    public $components = array('RequestHandler', 'Email', 'Auth', 'Session');
    public $helpers = array('Html', 'Js', 'Session', 'Form');
    public $uses = array('User');

    /**
     * Core Controller contruct 
     * User Authentication checked here .
     */
    function beforeFilter() {
        parent::beforeFilter();
        if (isset($this->params['admin'])) {
            $this->Auth->authenticate = array(
                AuthComponent::ALL => array('userModel' => 'User'),
                'Form' => array(
                    'fields' => array('username' => 'email')
                )
            );
            $this->Auth->authorize =false;
            $this->Auth->loginAction = array('controller' => 'users', 'action' => 'login');
            $this->Auth->fields = array('username' => 'email', 'password' => 'password');
            $this->Auth->loginRedirect = array('controller' => 'media', 'action' => 'index/');
            $this->Auth->autoRedirect = false;
            $this->Auth->logoutRedirect = '/logout';
            $this->Auth->loginError = 'Invalid e-mail / password combination. Please try again';
            $this->Auth->userScope = array('User.active = 1');
        } else {
          //  $this->Auth->allow('*');
            $this->redirect('/admin/users/login');
        }
    }

}

?>
