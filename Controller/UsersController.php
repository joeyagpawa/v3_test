<?php
class UsersController extends AppController {

    public $name    = 'Users';
    public $helpers = array();
    public $components  = array();
    var $uses    = array('User');
    
    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('login','logout','admin_login');
    }

    
    public function admin_login() {
        $this->layout='login';
         if ($this->Auth->login()) {
            if (!empty($this->data)) {
                if (empty($this->data['User']['remember_me'])) {
                }
                else {
                }
            }
            $this->Auth->loginRedirect = array('controller' => 'media', 'action' => 'index');
            $this->redirect($this->Auth->redirect());

        }
        $this->Session->setFlash( $this->Auth->loginError = 'There was a problem logging in. Please check your username and password.');
        if($this->action!='admin_login'){
            $this->redirect('/admin/users/login');
        }
    }

    /**
     * Logout and redirect to the login page .Delete the session and auth data
     *
     * @access public
     */
    public function admin_logout() {

        $this->Session->delete('User');
        $this->Session->destroy();
        $this->Auth->logout();
        $this->Session->setFlash('Good Bye');
        $this->redirect("/admin/users/login");

    }

    
}
?>
