<?php 
$m_class='';
$s_class='';

if($this->action=='admin_index'){
    $s_class='current';
}

$action=$this->action;
$name=$this->name;
?>

<div id="sidebar"><div id="sidebar-wrapper"> <!-- Sidebar with logo and menu -->
        <h1 id="sidebar-title"><a href="#">Admin</a></h1>
        <div id="profile-links">
            <?php echo $this->Html->link('View the Site', '/', array('title' => 'View the site')) . " | " . $this->Html->link('Sign Out', '/admin/users/logout', array('title' => 'Sign Out')); ?>
        </div>
        <ul id="main-nav">

            <li><?php echo $this->Html->link('Media', '#', array('class' => 'nav-top-item no-submenu')); ?>
                <ul>
                    <li><?php echo $this->Html->link('All Media', '/admin/media/',array('class'=>  ($action=='admin_index') ? 'current':null)); ?></li>
                    <li><?php echo $this->Html->link('Add New', '/admin/media/add_stepone', array('class' => ($action=='admin_add_stepone') ? 'current':null)); ?></li> <!-- Add class "current" to sub menu items also -->
                </ul>
            </li> <!-- Add the class "no-submenu" to menu items with no sub menu -->
            <li><?php echo $this->Html->link('Media Types', '#', array('class' => ($this->name=='MediaTypes')? 'nav-top-item  current':'nav-top-item ')); ?> 
                <ul>
                    <li><?php echo $this->Html->link('All Media Types', '/admin/media_types/',array('class'=>  ($action=='admin_index') ? 'current':null)); ?></li>
                    <li><?php echo $this->Html->link('Add New', '/admin/media_types/add', array('class' => ($action=='admin_add') ? 'current':null)); ?></li> <!-- Add class "current" to sub menu items also -->
                </ul>
            </li>
          
        </ul> <!-- End #main-nav -->

     

    </div></div> <!-- End #sidebar -->