<!-- Start Notifications -->
<?/*
<div class="notification attention png_bg">
    <a href="#" class="close"><img src="resources/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a>
    <div>
					Attention notification. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin vulputate, sapien quis fermentum luctus, libero.
    </div>
</div>

<div class="notification information png_bg">
    <a href="#" class="close"><img src="resources/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a>
    <div>
					Information notification. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin vulputate, sapien quis fermentum luctus, libero.
    </div>
</div>

<div class="notification success png_bg">
    <a href="#" class="close"><img src="resources/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a>
    <div>
					Success notification. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin vulputate, sapien quis fermentum luctus, libero.
    </div>
</div>

<div class="notification error png_bg">
    <a href="#" class="close"><img src="resources/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a>
    <div>
					Error notification. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin vulputate, sapien quis fermentum luctus, libero.
    </div>
</div>

<!-- End Notifications -->
*/?>
<div id="footer">
    <small>
        &#169; Copyright <?php echo date('Y') ; ?> | <a href="#">Top</a>
    </small>
</div><!-- End #footer -->
