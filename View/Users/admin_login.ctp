<?php echo $this->Form->create('User', array('action' => 'login'));?>
<div id="login-wrapper" class="png_bg">
    <div id="login-top">
        <h1>Microsemi Secure Panel</h1>
    </div> <!-- End #logn-top -->

    <div id="login-content">

        <?php if ($this->Session->read('Message.flash.message')) { ?>
            <div class="notification information png_bg">
                <div><?php $this->Session->flash(); ?></div>
            </div>
        <?php } ?>
        <p>
            <label>Email</label>
            <?php echo $this->Form->input('email', array('class' => 'text-input', 'label' => '')); ?>
        </p>
        <div class="clear"></div>
        <p>
            <label>Password</label>
            <?php echo $this->Form->input('password', array('class' => 'text-input', 'label' => '')); ?>
        </p>
        <div class="clear"></div>
        <p id="remember-password">

        </p>
        <div class="clear"></div>
        <p>
            <input class="button" type="submit" value="Sign In" />                        
        </p>

        <?php echo $this->Form->end(); ?>
    </div> <!-- End #login-content -->

</div> <!-- End #login-wrapper -->        
