<script>
	j(function() {
		j( "#MediaDate" ).datepicker();
        j('#MediaReleaseDate').datepicker();
        j('#MediaExpirationDate').datepicker();
	});
	</script>

<?php        echo $this->Session->flash(); ?>

<!-- End .shortcut-buttons-set -->
<div id="group_contents">
    <h2>Create New Media</h2>
    <p id="page-intro"></p>
    <div class="clear"></div> 
    <div class="content-box">
        <div id="user-box">
            <div class="content-box-header">
                <h3>Add Media Type</h3>
            </div>
            <div class="content-box-content">
                <div class="tab-content default-tab" id="tab1"> 
                    <div>
                        <?php echo $this->Form->create('Media',array('url'=>'/admin/media/edit/'.$this->data['Media']['id']));
                         echo '<p>'.$this->Form->input('name',array('class'=>'text-input medium-input','label'=>'Presentation Title')).'</p>';
                         echo '<p>'.$this->Form->input('description',array('class'=>'text-input textarea','style'=>'width:50% !important;','cols'=>50,'rows'=>2,'label'=>'Description')).'</p>';
                         echo '<p>'.$this->Form->input('keyword',array('class'=>'text-input textarea','style'=>'width:50% !important;', 'cols'=>50,'rows'=>2,'label'=>'Keyword')).'</p>';
                         echo '<p>'.$this->Form->input('price',array('class'=>'text-input small-input','label'=>'Price')).'</p>';
                         echo '<p><label>Running Times</label></p>';
                         echo $this->Form->input('hour',array('class'=>'text-input tiny-input','label'=>false,'div'=>false)).' &nbsp;Hours '.$this->Form->input('minute',array('class'=>'text-input tiny-input','label'=>false,'div'=>false)).' &nbsp;Minutes';;
                         echo '<p>'.$this->Form->input('date',array('class'=>'text-input tiny-input','type'=>'text', 'label'=>'Presentation Date','value'=>date('m/d/Y',strtotime($this->data['Media']['date'])))).'</p>';
                         echo '<p>'.$this->Form->input('release_date',array('class'=>'text-input tiny-input','type'=>'text','label'=>'Release Date','value'=>date('m/d/Y',strtotime($this->data['Media']['release_date'])))).'</p>';
                         echo '<p>'.$this->Form->input('expiration_date',array('class'=>'text-input tiny-input','type'=>'text','label'=>'Expiration Date','value'=>date('m/d/Y',strtotime($this->data['Media']['expiration_date'])))).'</p>';
                         echo $this->Form->hidden('media_type_id');
                         echo $this->Form->hidden('id');
                         echo '<p>'.$this->Form->submit('submit',array('div'=>false, 'class'=>'button')).'&nbsp;'.$this->Form->button('Cancel',array('div'=>false, 'class'=>'button','onclick'=>'window.location="/admin/pages/"')).'</p>';?>
                    </div>
                    <div class="clear"></div>
                </div>
            </div> 
        </div>
    </div>
</div>

