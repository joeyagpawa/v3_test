<?php
$this->Paginator->options(
        array('update' => 'group_contents',
            'url' => array('controller' => 'media', 'action' => 'index'),
            'indicator' => 'LoadingDiv'));
?>
<style>
    .checkbox{float: left;}
    label{float: left;margin-right: 10px;line-height: 20px;}
</style>
<script>
    function form_submit(){
         j('#MediaAdminIndexForm').submit(); 
    }
    
    
</script>
<div id="group_contents">
    <!-- Page Head -->
    <h2>All Media</h2>
    <p id="page-intro"></p>
    <div class="clear"></div>

    <div class="content-box"><!-- Start Content Box -->
        <div id="user-box">
            <div class="content-box-header">
                <h3>Select Media Types</h3>
            </div>
            <?php echo $this->Form->create('Media', array('url' => '/admin/media/')); ?>
            <div class="content-box-content">
                <div class="tab-content default-tab" id="tab1"> 
                    <?php foreach($mediaTypes as $i=>$mediatype){
                        $checked='';
                        if(!empty($checkedMediaTtypes))
                        if(in_array($i,$checkedMediaTtypes)){ $checked='checked';}else{$checked='';}
                        ?>
                    <div class="checkbox"><?php echo $this->Form->checkbox('Type', array('value'=>$i,'onchange'=>'form_submit();', 'name' => "data[Media][type][] ",  $checked)).$mediatype; ?></div>
                    <?php } ?>
                </div> 
                <div>&nbsp;</div>
            </div> 
            <?php echo $this->Form->end(); ?>
        </div>
    </div>

    <div class="content-box"><!-- Start Content Box -->
        <div id="user-box">
            <div class="content-box-header">
                <h3></h3>
            </div>
            <div class="content-box-content">
                <div class="tab-content default-tab" id="tab1"> 
                    <!-- This is the target div. id must match the href of this div's tab -->
                    <table>
                        <thead>
                            <tr>
                                <th><? echo $this->Paginator->sort('Media.name', 'Media Name'); ?></th>
                                <th><? echo $this->Paginator->sort('MediaType.name', 'Media Type'); ?></th>
                                <th><? echo $this->Paginator->sort('Media.price', 'Price'); ?></th>
                                <th><? echo $this->Paginator->sort('Media.date', 'Presentation Date'); ?></th>
                                <th><? echo $this->Paginator->sort('Media.release_date', 'Release Date'); ?></th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <td colspan="6">
                                    <div class="bulk-actions align-left">

                                    </div>
                                    <?php
                                    $total_pages = $this->Paginator->counter('{:pages}');
                                    if ($total_pages > 1) {
                                        ?>
                                        <div class="pagination">
                                            <?php echo $this->Paginator->prev('&laquo; ' . __('Previous', true), array('escape' => false, 'class' => 'prev'), null, array('class' => 'disabled', 'escape' => false)); ?>
                                            <?php echo $this->Paginator->numbers(array('class' => 'number')); ?>
                                        <?php echo $this->Paginator->next(__('Next', true) . ' &raquo;', array('escape' => false, 'class' => 'next'), null, array('class' => 'disabled', 'escape' => false)); ?>
                                        </div>
<?php } ?>
                                    <div class="clear"></div>
                                </td>
                            </tr>
                        </tfoot>
<?php foreach ($media as $page) { ?>
                            <tbody>
                                <tr  id="<?php echo 'page_' . $page['Media']['id']; ?>">
                                    <td><?php echo $page['Media']['name']; ?></td>
                                    <td><?php echo $page['MediaType']['name']; ?></td>
                                    <td><?php echo $page['Media']['price']; ?></td>
                                    <td><?php echo date('m-d-Y', strtotime($page['Media']['date'])); ?></td>
                                    <td><?php echo date('m-d-Y', strtotime($page['Media']['release_date'])); ?></td>
                                    <td>
                                        <?php
                                        echo $this->Html->link($this->Html->image('icons/pencil.png', array('alt' => 'Edit')), '/admin/media/edit/' . $page['Media']['id'], array('escape' => false, 'update' => 'page_' . $page['Media']['id']), null, null, false);
                                        ?>
                                        <?php
                                        echo $this->Html->link($this->Html->image('icons/cross.png', array('alt' => 'Delete')), '/admin/media/delete/' . $page['Media']['id'], array('escape' => false), 'Are you sure you want to delete this Media ? THIS OPERATION CANNOT BE UNDONE.', false);
                                        ?>
                                    </td>
                                </tr>
<?php } ?>
                        </tbody>

                    </table>

                </div> 
            </div> 
        </div>
    </div> 
</div>