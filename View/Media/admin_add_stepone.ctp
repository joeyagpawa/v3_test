<!-- End .shortcut-buttons-set -->
<div id="group_contents">
    <h2>Create New Media</h2>
    <p id="page-intro"></p>
    <div class="clear"></div> 
    <div class="content-box">
        <div id="user-box">
            <div class="content-box-header">
                <h3>Add Media Type</h3>
            </div>
            <div class="content-box-content">
                <div class="tab-content default-tab" id="tab1"> 
                    <p id="page-intro"></p>
                    <?php echo $this->Form->create('Media',array('url'=>'/admin/media/add_stepone','onsubmit'=>" return validate();"));?>
                    <div>
                        <ul class="shortcut-buttons-set"> <!-- Replace the icons URL's with your own -->

                            <?php foreach ($mediaTypes as $i=>$mediaType) { ?>
                                <li><a id="<?php echo $i;?>" class="shortcut-button" href="javascript:void(0);" onclick="setMediaType(<?php echo $i ;?>);"><span>
                                            <?php echo $mediaType ;?>
                                        </span></a></li>
                                   
                            <?php } ?>
                        </ul>
                    </div>
                    <div><?php echo  $this->Form->hidden('type_id');?></div>
                      <div class="clear"><?php echo '<p>'.$this->Form->submit('submit',array('div'=>false, 'class'=>'button')).'&nbsp;'.$this->Form->button('Cancel',array('div'=>false, 'class'=>'button','onclick'=>'window.location="/admin/media/index"')).'</p>';?></div>
                    <?php echo $this->Form->end();?>
                    <div class="clear"></div>
                </div>
            </div> 
        </div>
    </div>
</div>
<script>
function setMediaType(id){
    j('.shortcut-button').each(function(index){
        a_id=this.id;
        j('#'+a_id).css({border:'1px solid #CCCCCC'});
    });
    j('#MediaTypeId').val(id);
    j('#'+id).css({border:'1px solid red'});
}
function validate(){
    value=j('#MediaTypeId').val();
    if(value==''){
        alert('Please Select One Media Type');
        return false;
    }
    return true;
}
</script>