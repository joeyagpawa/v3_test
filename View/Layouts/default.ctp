<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <?php echo $this->Html->charset(); ?>
        <title>
            <?php echo 'Microsemi  »' . $title_for_layout; ?>
        </title>
        <?php
        echo $this->Html->meta('icon');
        echo $this->Html->css('reset');
        echo $this->Html->css('admin');
        echo $this->Html->css('invalid');
        echo $this->Html->css('jquery/base/jquery.ui.all');
        echo $this->Html->script(array('prototype-1.6.0.3', 'prototype-functions', 'scriptaculous',
            ));
        echo $this->Html->script('jquery/jquery-1.6.2');
        echo $this->Html->script('jquery-ui-1.8.9.min');
        echo $this->Html->script('jquery.wysiwyg');
        $this->Js->JqueryEngine->jQueryObject = 'j';
        echo $this->Html->scriptBlock('var j = jQuery.noConflict();');
         echo $this->Html->script(array('admin.configuration'));
        ?>
    </head>
    <body>
        <div id="body-wrapper">
                <?php  echo $this->element('admin' . DS . 'left_side_bar'); ?>

                <div id="main-content"> <!-- Main Content Section with everything -->

                    <?php // e(View::renderElement('common'.DS.'top_content')); ?>
                    <?php echo $this->Session->flash(); ?>
                    <?php echo $content_for_layout; ?>
                    <div class="clear"></div>
                    <?php echo $this->element('admin' . DS . 'footer'); ?>
                </div> 

        </div>


        <?php echo $this->element('sql_dump'); ?>
    </body>
</html>