<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <?php echo $this->Html->charset(); ?>
        <title>
            <?php echo 'Microsemi  »' . $title_for_layout; ?>
        </title>
        <?php
        echo $this->Html->meta('icon');
        echo $this->Html->css('reset');
        echo $this->Html->css('admin');

        echo $this->Html->script(array('prototype-1.6.0.3', 'prototype-functions', 'scriptaculous',
            'jquery/jquery-1.6.2', 'jquery/jquery-ui-1.8.16.custom', 'swfobject'));
        $this->Js->JqueryEngine->jQueryObject = 'j';
        echo $this->Html->scriptBlock('var j = jQuery.noConflict();');
        ?>
    </head>
    <body id="login">             
            <?php echo $content_for_layout; ?>
             <?php echo $this->element('sql_dump'); ?>
    </body>
</html>