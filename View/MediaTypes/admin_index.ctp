<?php
$this->Paginator->options(
        array('update' => 'group_contents',
            'url' => array('controller' => 'MediaTypes', 'action' => 'index'),
            'indicator' => 'LoadingDiv'));
?>
<div id="group_contents">
    <!-- Page Head -->
    <h2>Media Types</h2>
    <p id="page-intro"></p>
    <div class="clear"></div> 
    <div class="content-box"><!-- Start Content Box -->
        <div id="user-box">
            <div class="content-box-header">
                <h3></h3>
            </div>
            <div class="content-box-content">
                <div class="tab-content default-tab" id="tab1"> 
                    <!-- This is the target div. id must match the href of this div's tab -->
                    <table>
                        <thead>
                            <tr>
                                <th>Type Name</th>
                                <th>Created</th>  
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <td colspan="6">
                                    <div class="bulk-actions align-left">

                                    </div>
                                    <?php 
                                    $total_pages=$this->Paginator->counter('{:pages}');
                                    if($total_pages>1){ ?>
                                    <div class="pagination">
                                        <?php echo $this->Paginator->prev('&laquo; ' . __('Previous', true), array('escape' => false, 'class' => 'prev'), null, array('class' => 'disabled', 'escape' => false)); ?>
                                        <?php echo $this->Paginator->numbers(array('class' => 'number')); ?>
                                        <?php echo $this->Paginator->next(__('Next', true) . ' &raquo;', array('escape' => false, 'class' => 'next'), null, array('class' => 'disabled', 'escape' => false)); ?>
                                    </div>
                                    <?php } ?>
                                    <div class="clear"></div>
                                </td>
                            </tr>
                        </tfoot>
                        <?php foreach ($mediaTypes as $page) { ?>
                            <tbody>
                                <tr  id="<?php echo 'page_' . $page['MediaType']['id']; ?>">
                                    <td><?php echo $page['MediaType']['name']; ?></td>
                                    <td><?php echo date('m-d-Y', strtotime($page['MediaType']['created'])); ?></td>
                                    <td>
                                        <?php
                                        echo $this->Html->link($this->Html->image('icons/pencil.png', array('alt' => 'Edit')), '/admin/media_types/edit/' . $page['MediaType']['id'], array('escape'=>false, 'update' => 'page_' . $page['MediaType']['id']), null, null, false);
                                        ?>
                                        <?php
                                        echo $this->Html->link($this->Html->image('icons/cross.png', array('alt' => 'Delete')), '/admin/media_types/delete/' . $page['MediaType']['id'], array('escape'=>false), 'Are you sure you want to delete this Media Type? THIS OPERATION CANNOT BE UNDONE.', false);
                                        ?>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>

                    </table>

                </div> 
            </div> 
        </div>
    </div> 
</div>