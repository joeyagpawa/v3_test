<?php echo $this->Session->flash(); ?>

<!-- End .shortcut-buttons-set -->
<div id="group_contents">
    <h2>Create New Media</h2>
    <p id="page-intro"></p>
    <div class="clear"></div> 
    <div class="content-box">
        <div id="user-box">
            <div class="content-box-header">
                <h3>Add Media Type</h3>
            </div>
            <div class="content-box-content">
                <div class="tab-content default-tab" id="tab1"> 
                    <div>
                        <?php echo $this->Form->create('MediaType', array('url' => '/admin/media_types/edit/'.$this->data['MediaType']['id'])); ?>
                        <?php echo '<p>' . $this->Form->input('name', array('class' => 'text-input medium-input', 'label' => 'Title')) . '</p>'; ?>
                        <?php echo '<p>' . $this->Form->input('description', array('class' => 'text-input textarea wysiwyg', 'cols' => 79, 'rows' => 5, 'label' => 'Description')) . '</p>'; ?>
                        <?php echo $this->Form->hidden('id');?>
                        <?php echo '<p>' . $this->Form->submit('submit', array('div' => false, 'class' => 'button')) . '&nbsp;' . $this->Form->button('Cancel', array('div' => false, 'class' => 'button', 'onclick' => 'window.location="/admin/pages/"')) . '</p>'; ?>
                    </div>
                    <div class="clear"></div>
                </div>
            </div> 
        </div>
    </div>
</div>