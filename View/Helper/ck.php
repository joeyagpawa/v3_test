<?php 
	class CkHelper extends Helper {
		
		function load($id, $initialValue) {
			// Include CKEditor class.
			include_once "js/ckeditor/ckeditor.php";
			include_once 'js/ckeditor/ckfinder/ckfinder.php';
			// Create class instance.
            $CKEditor =new CKEditor();
			// Path to CKEditor directory, ideally instead of relative dir, use an absolute path:
			//   $CKEditor->basePath = '/ckeditor/'
			// If not set, CKEditor will try to detect the correct path.
		//	$CKEditor->basePath = 'js/ckeditor/';
			CKFinder::SetupCKEditor($CKEditor, '/js/ckeditor/ckfinder/');
			// Create textarea element and attach CKEditor to it.
			$CKEditor->editor($id, $initialValue);
		}
	}
?>