<?php

class Media extends AppModel {

    public $name = 'Media';
    public $useTable = 'media';
    public $validate = array(
        'name' => array(
            'between' => array(
                'rule' => array('between', 3, 25),
                'message' => "Between 3 to 25 characters . And can't be empty"
            ),
            'checkDuplicateName' => array(
                'rule' => array('checkDuplicateName'),
                'message' => 'Duplication Media Name'
            )
        ),
        'date' => array(
            'rule' => array('date', 'mdy'),
            'message' => 'Enter a valid date',
            'allowEmpty' => true
        ),
        'description' => array(
            'rule' => array('minLength', 15),
            'message' => 'Minimum 15 characters',
            'allowEmpty' => false
        ),
        'media_type_id' => array(
            'rule' => 'notEmpty',
            'message' => "Media Type Can't be empty"
        )
    );
    public $belongsTo = array('MediaType');

    /**
     *
     * @param type $options 
     */
    public function beforeSave($options = array()) {
        parent::beforeSave($options);
        $this->data['Media']['date'] = $this->convertDateToDBFormat($this->data['Media']['date']);
        $this->data['Media']['release_date'] = $this->convertDateToDBFormat($this->data['Media']['release_date']);
        $this->data['Media']['expiration_date'] = $this->convertDateToDBFormat($this->data['Media']['expiration_date']);
        return true;
    }

    /**
     * Check the duplicate validations
     * @return type 
     */
    function checkDuplicateName() {

        $check = $this->find('first', array('conditions' => array('Media.name' => $this->data['Media']['name']), 'recursive' => -1));
        if (!empty($this->id) && ($this->data['Media']['name'] == $check['Media']['name'])) {
            return true;
        }
        if (empty($check))
            return true;
        else
            return false;
    }

    /**
     * Convert a MM-DD-YYYY date to YYYY-MM-DD
     * 
     * @param string $date
     * @return string
     */
    function convertDateToDBFormat($date = null) {
        $parts = explode('/', $date);
        return $parts[2] . '-' . $parts[0] . '-' . $parts[1];
        return $date;
    }

    /**
     * Convert a YYYY-MM-DD date to MM-DD-YYYY
     * 
     * @param string $date
     * @return string
     */
    function convertDateFromDBFormat($date = null) {
        $parts = explode('/', $date);
        return $parts[1] . '/' . $parts[2] . '/' . $parts[0];
        return $date;
    }

}

?>