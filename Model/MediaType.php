<?php

class MediaType extends AppModel {

    public $name = 'MediaType';
    public $validate = array(
        'name' => array(
            'between' => array(
                'rule' => array('between', 3, 15),
                'message' => 'Between 3 to 15 characters'
            )
        )
    );

   

}

?>