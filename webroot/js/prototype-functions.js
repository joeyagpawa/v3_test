/*
prototypeUtils.js from http://jehiah.com/
Licensed under Creative Commons.
version 1.0 December 20 2005

Contains:
+ Form.Element.setValue()
+ unpackToForm()

*/

/* Form.Element.setValue("fieldname/id","valueToSet") */
Form.Element.setValue = function(element,newValue) {
    element_id = element;
    element = $(element);
    if (!element){element = document.getElementsByName(element_id)[0];}
    if (!element){return false;}
    var method = element.tagName.toLowerCase();
    var parameter = Form.Element.SetSerializers[method](element,newValue);
}

Form.Element.SetSerializers = {
  input: function(element,newValue) {
    switch (element.type.toLowerCase()) {
      case 'submit':
      case 'hidden':
      case 'password':
      case 'text':
        return Form.Element.SetSerializers.textarea(element,newValue);
      case 'checkbox':
      case 'radio':
        return Form.Element.SetSerializers.inputSelector(element,newValue);
    }
    return false;
  },

  inputSelector: function(element,newValue) {
    fields = document.getElementsByName(element.name);
    for (var i=0;i<fields.length;i++){
      if (fields[i].value == newValue){
        fields[i].checked = true;
      }
    }
  },

  textarea: function(element,newValue) {
    element.value = newValue;
  },

  select: function(element,newValue) {
    var value = '', opt, index = element.selectedIndex;
    for (var i=0;i< element.options.length;i++){
      if (element.options[i].value == newValue){
        element.selectedIndex = i;
        return true;
      }        
    }
  }
}

function unpackToForm(data){
   for (i in data){
     Form.Element.setValue(i,data[i].toString());
   }
}

/* Jeven specific functions */

function findParent() {
	url = '/standards/findparent/' + $('StandardSubjectId').getValue() + '-' + $('StandardGradelevelId').getValue() + '-' + $('StandardState').getValue();
	updateDiv('parentStandard', url);
}

function updateDiv(div2Update, updateWith) {
	new Ajax.Updater(div2Update, updateWith, {asynchronous:true, evalScripts:true, requestHeaders:['X-Update', div2Update]});
}

function selectUpdater(modelId, url, div2Update) {
	updateDiv(div2Update, url + modelId);
}

function doubleFieldUpdater(modelId, url, div2Update) {
	alert('hello');
}

function ajaxSubmit(div2Update, url, grade, postField) {
	var mygrade = postField + '=' + grade;
	new Ajax.Updater(div2Update, url, {asynchronous:true, method:'post', postBody:mygrade, requestHeaders:['X-Update', div2Update]});
}

function validateGrade(grade, input, oldFieldValue) {
	if ((grade > 120) || (grade < 0) || (!isInteger(grade))) {
		$(input).setStyle({backgroundColor: '#FFCFCF', color: '#000', border: '1px solid #F10808'});
	}
	else {
		$(input).setStyle({backgroundColor: '#fff'});
		if (grade != oldFieldValue) {
			$(input).setStyle({color: '#000', border: '1px solid #000'});			
		}
	}
}

function isInteger(s) {
   var i;
   if (isEmpty(s))
   if (isInteger.arguments.length == 1) return 0;
   else return (isInteger.arguments[1] == true);
   for (i = 0; i < s.length; i++) {
      var c = s.charAt(i);
      if (!isDigit(c)) return false;
   }
   return true;
}

function isEmpty(s) {
   return ((s == null) || (s.length == 0))
}

function isDigit (c) {
   return ((c >= "0") && (c <= "9"))
}
